import Vue from 'vue';
import VueRouter from 'vue-router';
// 页面
import home from '../views/home';
import help from '../views/help';
import error from '../views/error';

Vue.use(VueRouter);

let router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'on',
  routes: [
    {
      path: '',
      redirect: '/home'
    },
    {
      path: '/home',
      component: home
    },
    {
      path: '/help',
      component: help
    },
    {
      path: '*',
      component: error
    }
  ]
});

export default router;
