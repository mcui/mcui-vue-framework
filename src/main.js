import Vue from 'vue';
import root from './root';
// 数据中心
import store from './store';
// 全局公共样式
import '@/assets/css/main.scss';

Vue.config.productionTip = false;

new Vue({
  store,
  render: h => h(root)
}).$mount('#root');
