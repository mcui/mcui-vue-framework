import Vue from 'vue';
import Vuex from 'vuex';

// 使用vuex
Vue.use(Vuex);

//实例化vuex.Store对象
let store = new Vuex.Store({
  state: {
    page: {
      pageName: 'home'
    }
  },
  mutations: {}
});

export default store;
